from django.urls import path

from .api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    path("attendees/", api_list_attendees, name="api_create_attendees"),

    path(
        "conferences/<int:conference_vo_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
    path("attendees/<int:id>/", api_show_attendee, name="api_show_attendee"),
]

# added this new path below to link the views to urls
# path("attendees/", api_list_attendees, name="api_create_attendees"),

# then updated parameter name for
# the other path in order to reflect new changes 

# path(
#    "conferences/<int:conference_vo_id>/attendees/",
#    api_list_attendees,
#    name="api_list_attendees",
#    ),
