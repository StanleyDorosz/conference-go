from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
from .acls import get_photo, get_weather_data
import json

# normally we put our encoder classes up top
# but putting above the function we are going tp use it with
# is fine


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


# using decorator to make it so only
#requests that can be put through are get and post
@require_http_methods(["GET", "POST"])
# created a function for requesting the list of conferences
# as well as posting more
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
# if the request we get is a "GET"
        conferences = Conference.objects.all()
# note : when model instance is created it is stored in a QuerySet
# then we are going to create a variable that holds the value
# which grabs all of the conference models instances

# this return is returning a JsonResponse
# with the conferences and Encoder as parameters *see docs below*
# https://docs.djangoproject.com/en/4.0/ref/request-response/#changing-the-default-json-encoder
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
# since its an else statement it assumes that it is a post request
# content variable is created and extracts
# content from the request body and puts it into a python dictionary

# this is for line 62
# attempting to retrieve a location instance from our database based
# on the id provided in our content variable
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
# line 64 is setting our location
#value inside the python dictionary to the value we grabbed on line 63

# except block is running if there is an invalid location id
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
# line 77 is creating a new conference object with
# all of the content in the dictionary

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
#this is returning a JsonResponse with the
# conference variable as our data Encoded with
# our ConferenceDetailEncoder
# and safe=False due to our data being a model instance

# see docs for ModelEncoder info
# https://docs.python.org/3/library/json.html#json.JSONEncoder
# https://learn-2.galvanize.com/cohorts/3753/blocks/1873/content_files/build/02-more-ddd/71-building-your-json-library.md

# This class is something we created to Encode python dictionary's
# or any of our python data

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url"
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

# added picture_url to properties of our encoder for that specific model

# this function is inside the LocationDetailEncoder
# see docs/learn for more info
# https://learn-2.galvanize.com/cohorts/3753/blocks/1873/content_files/build/02-more-ddd/71-building-your-json-library.md
# this function get_extra_data is created and accepts two parameters
# in order to access a separate
# models field through a foreign key and returns that extra data
# in a python dictionary

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

# this is doing the same thing whatever
# you put in the properties is what we will encode
# name is the location is this encoder


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

# the encoders field being passed is in this
# ConferenceDetailEncoder has the same functionality as get_extra_data
# also makes it easier because already have an
# Encoder with the data that we want


# This api_show_conference was created in order
# to return a JsonResponse displaying one individual conference
# with all of its data

@require_http_methods({"GET", "PUT", "DELETE"})
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: # PUT
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )




# this is the same process as api_list_conferences function above
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """


    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            content["picture_url"] = get_photo(content["city"], content["state"].name)
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

# we added content variable storing the new field we added to the model as a key and utilized our get_photo function

# using decorator to make it so only
# requests that can be put through are get and put and delete

@require_http_methods(["DELETE", "GET", "PUT"])

# we passed request as a parameter of our api_show_location function as well as id
# in order to use those two parameters in our function
def api_show_location(request, id):
    """


    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
# if the request we get is a "GET"
    if request.method == "GET":

# creating a variable that is grabbing a specific location instance from
# the Location model filtered by id
        location = Location.objects.get(id=id)

# #this is returning a JsonResponse with the
# location variable as our data Encoded with
# our LocationDetailEncoder
# and safe=False due to our data being a model instance and not a QuerySet
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
# else if the request.method is delete
    elif request.method == "DELETE":

# Grabs a specific QuerySet of a Location instance,
# filtered by the specified id. We then call the
# .delete method which returns a tuple that contains a
# count of the objects deleted, and a dictionary of what was deleted.
# We are storing the first value in our "count" variable,
# and using _ to disregard the second value we get. since in this case we
# do not need to display the details of what was deleted
        count, _ = Location.objects.filter(id=id).delete()

# this is returning a JsonResponse which indicates whether
#  or not our instance was deleted, count > 0 returns a boolean value
# outputs True if count of number of instances deleted is greater than 0
# else it returns false because 0 is not greater than zero
        return JsonResponse({"deleted": count > 0})

# this else is a "PUT"
    else:
# this does the same thing as any of the other post functions
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

# accessing the Location model and using the objects manager
# (which is specific to Python Models) to filter through the specific ids
# to then update all of the content (** unpacks a dictionary)
        Location.objects.filter(id=id).update(**content)

# already been through the code below, see above function(s) for info
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# try and excepts are for error handling
