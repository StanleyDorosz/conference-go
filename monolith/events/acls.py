import requests
import json
from .keys import PEXELS_API, OPEN_WEATHER_API

# after importing the api READ THE DOCS FOR THAT API

def get_photo(city, state):

    url = 'https://api.pexels.com/v1/search'

    authorization = {
        "Authorization": PEXELS_API
    }

    parameters = {
        "query": f"{city}, {state}"
    }

    response = requests.get(url, params=parameters, headers=authorization)

    content = json.loads(response.content)
    return content["photos"][0]["src"]["original"]


def get_weather_data(city, state):

    params = {
        "q": (city, state),
        "appid": OPEN_WEATHER_API,
        "limit": 1
    }

    url = 'http://api.openweathermap.org/geo/1.0/direct'

    response = requests.get(url, params=params)

    coordinates = response.json()

    weather_params = {
        "lat": coordinates[0]["lat"],
        "lon": coordinates[0]["lon"],
        "appid": OPEN_WEATHER_API,
        "units": "imperial",
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(weather_url, params=weather_params)

    local_weather=weather_response.json()

    try:
        return {
            "description": local_weather["weather"][0]["description"],
            "temperature": local_weather["main"]["temp"]
        }
    except (KeyError, IndexError):
        return {"weather": None}


# turn the response that is in json from into python

    # printing response for testing
    # print(response.content)


    # use requests library to get a photo

    # create a dictionary of the data we would like to
    # use from the pexels response

# below the url we created the authorization variable
#  that hold a dictionary
# with the key Authorization holding our API KEY


# when indexing a dictionary call by key when indexing lists call by index ex . [0]
# calling the function with input parameters
# get_photo("Austin", "Texas")

# look for required parameters or parameters you'd like to include

# once you are sure it works and you get  200 REQUEST
# and all queries or parameters are set and
# its giving back the data you want
# then you can go back into your code , format your request
# and send it

# all apis are case sensitive

# calling function for testing
# get_photo("Austin", "Texas")


# anytime looking for api that you want to use be
# sure to check parameters/queries or query parameters
# (look for these keywords)
# just to figure out how to use it

# generally looking for the url and the
# parameters in order to use it

# How to use an api in general

# 1. make request
    # - get url
    # - get query parameters
    # - figure out how to authenticate


# 2. send request

# 3. get data from response


# anytime you get a 401 http error
# (whether in insomnia or postman or in general)
# just means unauthorized to use that api
# means theres an issue with your key or using key wrong or dont have a key


# not every api requires Authorization header
# and key but for this project we need to add it
# in our insomnia/request in general
